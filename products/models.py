from django.db import models


# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=120)  # max length is required
    description = models.TextField(default='')
    price = models.DecimalField(decimal_places=2, max_digits=1000)
    summary = models.TextField(default='this is cool !!! ')
    featured = models.TextField(default='')
