from django.contrib import admin

# Register your models here.
from .models import Product
# register model for admin portal
admin.site.register(Product)
