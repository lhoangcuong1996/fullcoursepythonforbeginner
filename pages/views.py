# import to response view from request
from django.http import HttpResponse
from django.shortcuts import render


# Create your views here.
def home_view(request, *args, **kwargs):
    # content WSGIRequest object , include some data such as method type , url ,params , ...
    # authentication is handled from here or some validation
    print(request.user)
    # you can use HttpRequest or render function to response a html file for client

    # return HttpResponse('<h1>Hello<h1>')

    # if using a html template use have to define the path of template on setting root app
    return render(request, 'template_home.html', {})


def contact_view(request, *args, **kwargs):
    # return HttpResponse('<h1>Contact<h1>')
    return render(request, 'template_contact.html', {})
